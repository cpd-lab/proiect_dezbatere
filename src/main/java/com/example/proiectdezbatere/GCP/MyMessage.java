package com.example.proiectdezbatere.GCP;

import org.springframework.stereotype.Component;
/*
*Componenta care pastreaza ultimul mesaj publicat de aplicatie pe un topic
* Este creata cu scopul de a nu-l afisa cand face subscribe la acel topic
 */
@Component
public class MyMessage {
    private StringBuffer lastMessage;

    public MyMessage(){
        lastMessage=new StringBuffer(" ");
    }

    public StringBuffer getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(StringBuffer lastMessage) {
        this.lastMessage = lastMessage;
    }
}
