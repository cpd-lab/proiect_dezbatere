package com.example.proiectdezbatere.GCP;

import com.example.proiectdezbatere.Sockets.SocketConnection;
import com.example.proiectdezbatere.Sockets.Token;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/*
* In aceasta componenta se face subscribe la topicurile pe care le urmareste aplicatia
* Tot aici se face publish cu datele introduse de utilizator, atunci cand aplicatia are tokenul
 */
@Component
public class PubSub {


    private final PubSubTemplate pubSubTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(PubSub.class);
    private final ConfigurationGCP gcp;
    private final Token token;
    private final MyMessage lastMessage;
    private final SocketConnection socketConnection;

    public PubSub(PubSubTemplate pubSubTemplate, ConfigurationGCP gcp, Token token, MyMessage lastMessage, SocketConnection socketConnection) throws IOException {
        this.gcp = gcp;
        this.token = token;
        this.lastMessage = lastMessage;
        this.socketConnection = socketConnection;
        String[] subs=gcp.getSubscriptions().split(",");
        this.pubSubTemplate = pubSubTemplate;
        for(String subscriptionName: subs) {
            pubSubTemplate.subscribe(subscriptionName, (message) -> {
                if(!message.getPubsubMessage().getData().toStringUtf8().equalsIgnoreCase(lastMessage.getLastMessage().toString())) {
                    String[] aux=subscriptionName.split("-");
                    LOGGER.info(aux[0] + " : " + message.getPubsubMessage().getData().toStringUtf8());
                    message.ack();
                }
                else
                {
                    message.ack();
                }
            });
        }
        keepAlive();
    }

    public void keepAlive() throws IOException {
            String[] topics = gcp.getTopics().split(",");
            Scanner scan = new Scanner(System.in);
            while (true) {
                if (token.getPresent().get()) {
              //  System.out.println("Mesaj de transmis:");
                String mesaj = scan.nextLine();
                if(!token.getPresent().get())
                    continue;
                else if(!mesaj.isEmpty())
                {
                    if (mesaj.contains("gata"))
                    {
                        socketConnection.getClientIesire().dezbate(mesaj);
                        
                        break;
                    }
                    else if(mesaj.equalsIgnoreCase("Take token")){
                        token.setPresent(new AtomicBoolean(false));
                        socketConnection.getClientIesire().dezbate(mesaj);
                        LOGGER.info("Token lost");
                    }
                    String[] elemMesaj= mesaj.split(":");
                    for(String top:topics){
                        if(elemMesaj[0].equalsIgnoreCase(top))
                        {
                            pubSubTemplate.publish(top, elemMesaj[1]);
                            lastMessage.setLastMessage(new StringBuffer(elemMesaj[1]));
                        }
                    }
                }
            }
        }

    }
}
