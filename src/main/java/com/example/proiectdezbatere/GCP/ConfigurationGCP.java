package com.example.proiectdezbatere.GCP;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
/*
 *Aceasta clasa cu configuratii preia datele legate de PubSub din aplication properties
 */
@Configuration
public class ConfigurationGCP {
    @Value( "${topics}" )
    private  String topics;

    @Value( "${subscriptions}" )
    private  String subscriptions;

    public String getSubscriptions() {
        return subscriptions;
    }

    public String getTopics() {
        return topics;
    }
}
