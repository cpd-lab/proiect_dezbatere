package com.example.proiectdezbatere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableScheduling
@SpringBootApplication
public class ProiectdezbatereApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProiectdezbatereApplication.class, args);

    }

}
