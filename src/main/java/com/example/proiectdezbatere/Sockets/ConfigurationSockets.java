package com.example.proiectdezbatere.Sockets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/*
*Aceasta clasa cu configuratii preia datele legate de socketuri din aplication properties
 */
@Configuration
public class ConfigurationSockets {
    @Value( "${intrare}" )
    private  int intrare;

    @Value( "${iesire}" )
    private  int iesire;

    @Value( "${appId}" )
    private  int id;

    public int getIntrare(){
        return intrare;
    }

    public int getIesire(){
        return iesire;
    }

    public int getId(){
        return id;
    }

}
