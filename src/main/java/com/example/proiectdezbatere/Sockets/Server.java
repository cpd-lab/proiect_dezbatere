package com.example.proiectdezbatere.Sockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    private int port;
    public Server(int port, DecisionDetails decision, ConfigurationSockets configurationSockets, Token token){
        this.port=port;
        this.decision = decision;
        this.configurationSockets = configurationSockets;
        this.token = token;
    }

    private static final org.slf4j.Logger LOGGER = (Logger) LoggerFactory.getLogger(Server.class);
    private final DecisionDetails decision;
    private final ConfigurationSockets configurationSockets;
    private final Token token;


    @Override
    public void run(){
        ServerSocket DezbatereChannel = null;
        try {
            DezbatereChannel = new ServerSocket(port);
            while (true)
            {
                Socket socketForClient = DezbatereChannel.accept();
                new ClientHandler(socketForClient, decision, token, configurationSockets).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
