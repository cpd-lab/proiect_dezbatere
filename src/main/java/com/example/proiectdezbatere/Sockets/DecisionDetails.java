package com.example.proiectdezbatere.Sockets;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;
/*
* In acesta clasa se pastreaza informatiile despre seful initial al inelului.
* Seful este aplicatia cu cel mai mare id, el o sa aiba primul tokenul.
 */

@Component
public class DecisionDetails {

    public DecisionDetails(){
        bossId= new AtomicInteger(-1);
    }

    private AtomicInteger bossId;

    public void setBossId(AtomicInteger bossId) {
        this.bossId = bossId;
    }

    public AtomicInteger getBossId() {
        return bossId;
    }
}
