package com.example.proiectdezbatere.Sockets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    Socket socket;
    private PrintWriter toServer;

    public void start(String ip, int port) throws IOException {
        socket = new Socket(ip, port);
       toServer = new PrintWriter(socket.getOutputStream(), true);
    }

    public void dezbate(String text) throws IOException {
        toServer.println(text);
    }


    public void stop() throws IOException {
        toServer.close();
        socket.close();
    }
}
