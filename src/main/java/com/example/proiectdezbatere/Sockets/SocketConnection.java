package com.example.proiectdezbatere.Sockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/*
* In aceasta clasa se stabileste legatura dintre socketuri si se decide cine este seful in inel, iar el o sa primeasca tokenul
 */
@Component
public class SocketConnection {

    private final ConfigurationSockets configuration;
    private final DecisionDetails decision;
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketConnection.class);
    private  Client clientIesire;
    private  final Token token;

    SocketConnection(ConfigurationSockets configuration, DecisionDetails decision, Token token) throws IOException {
        this.configuration = configuration;
        this.decision = decision;
        this.token = token;

        Server s=new Server(configuration.getIntrare(), this.decision, configuration, this.token);
        s.start();


        clientIesire=new Client();
        long startTime = System.currentTimeMillis();
        long endTime;
        while (true) {
            try{
                clientIesire.start("127.0.0.1", configuration.getIesire());
                LOGGER.info("connected to output socket");
                break;
            }
            catch (Exception e){
                LOGGER.info(" still trying to connect");
            }
            endTime = System.currentTimeMillis();
            if(endTime-startTime>20000)//20 sec
            {
                LOGGER.info("not connected");
                break;
            }
        }

        decideBoss();
        token.start();
        if(configuration.getId()==decision.getBossId().get())
        {
            token.setPresent(new AtomicBoolean(true));
            token.setArriveTime(new AtomicLong(System.currentTimeMillis()));
            LOGGER.info("I have the token!");
        }
    }

    public void decideBoss() throws IOException {
        clientIesire.dezbate("Who is the boss? my id="+configuration.getId());
        long startTime = System.currentTimeMillis();
        LOGGER.info("trying to find the boss");
        while(decision.getBossId().get()==-1)
        {
            long endTime = System.currentTimeMillis();
            if(endTime-startTime>200)//2 sec
            {
                LOGGER.info("trying to find the boss");
                startTime=endTime;
            }
        }
        LOGGER.info("found the boss, id= "+decision.getBossId().get());
        if(configuration.getId()!=decision.getBossId().get())
            clientIesire.dezbate("Boss id="+decision.getBossId().get());

    }

    public Client getClientIesire() {
        return clientIesire;
    }
}
