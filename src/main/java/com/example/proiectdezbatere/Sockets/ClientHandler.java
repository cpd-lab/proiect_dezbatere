package com.example.proiectdezbatere.Sockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class ClientHandler extends Thread{

    private Socket socketForClient;
    private BufferedReader fromClient;
    private final DecisionDetails decision;
    private final Token token;
    private final ConfigurationSockets configurationSockets;
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);


    public ClientHandler(Socket socket, DecisionDetails decision, Token token, ConfigurationSockets configurationSockets){
        this.socketForClient=socket;
        this.decision = decision;
        this.token = token;
        this.configurationSockets = configurationSockets;
    }

    @Override
    public void run(){

        try {
            fromClient = new BufferedReader(new InputStreamReader(socketForClient.getInputStream()));


            String message;

            while ((message = fromClient.readLine()) != null) {
                if (".".equals(message)) {
                    break;
                }
                else if (message.contains("Who is the boss?")){ //mesajul primit este Who is the boss? my id=...
                    String[] parts= message.split("=");
                    int id=Integer.parseInt(parts[1]);
                    if(configurationSockets.getId()<id)
                    {
                        decision.setBossId(new AtomicInteger(id));
                    }
                }
                else if (message.contains("Boss id=")){ //mesajul primit este Boss id=...
                    String[] parts= message.split("=");
                    int bossId=Integer.parseInt(parts[1]);
                    decision.setBossId(new AtomicInteger(bossId));
                }
                else if(message.equalsIgnoreCase("Take token")){
                    token.setArriveTime(new AtomicLong(System.currentTimeMillis()));
                    token.setPresent(new AtomicBoolean(true));
                    LOGGER.info("Token received!");
                }
                else if(message.equalsIgnoreCase("gata"))
                    break;
                else{
                    System.out.println(message+" in client handler "+socketForClient.getLocalPort());
                }
            }

            socketForClient.close();
            fromClient.close();

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}

