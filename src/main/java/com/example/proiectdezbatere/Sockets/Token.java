package com.example.proiectdezbatere.Sockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
/*
*Aceasta clasa pastreaza evidenta tokenului, daca aplicatia il detine sau nu
* Dupa primirea tokenului la maxim 30 de secunde il trimite la urmatoarea aplicatie
 */

@Component
public class Token extends Thread {
    private AtomicLong arriveTime;//time when token arrived to app
    private AtomicBoolean present;
    private static final Logger LOGGER = LoggerFactory.getLogger(Token.class);


    @Value( "${iesire}" )
    private  int iesire;

    public Token() throws IOException {
        arriveTime= new AtomicLong(0);
        present=new AtomicBoolean(false);
        present=new AtomicBoolean(false);

    }

    public AtomicBoolean getPresent() {
        return present;
    }

    public AtomicLong getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(AtomicLong arriveTime) {
        this.arriveTime = arriveTime;
    }

    public void setPresent(AtomicBoolean present) {
        this.present = present;
    }




    @Override
    public void run() {
        long currentTime;
        while(true){
            if(present.get())
            {
                currentTime=System.currentTimeMillis();
                if(currentTime-arriveTime.get()>=30000)//30 sec
                {

                    try {
                        setPresent(new AtomicBoolean(false));
                        Client clientIesire=new Client();
                        clientIesire.start("127.0.0.1", iesire);
                        clientIesire.dezbate("Take token");
                        LOGGER.info("Token lost");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }



}
