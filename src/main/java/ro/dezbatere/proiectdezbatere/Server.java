package ro.dezbatere.proiectdezbatere;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private String descriere;
    private int port;
    public Server(String descriere, int port){
       this.descriere=descriere;
       this.port=port;
    }

    private static final org.slf4j.Logger LOGGER = (Logger) LoggerFactory.getLogger(Server.class);

    public void start(){
        ServerSocket DezbatereChannel = null;
        try {
            DezbatereChannel = new ServerSocket(50);
            while (true)
            {
                Socket socketForClient = DezbatereChannel.accept();
                // new ClientHandler(socketForClient).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Scheduled(fixedDelay = 5000)
    public void keepAlive() {
        LOGGER.info("keep alive");
    }

}
