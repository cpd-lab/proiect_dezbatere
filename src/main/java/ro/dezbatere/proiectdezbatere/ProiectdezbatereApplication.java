package ro.dezbatere.proiectdezbatere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ProiectdezbatereApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProiectdezbatereApplication.class, args);

        Server s=new Server("canalIntrare",5050);
        //Server s1=new Server("canalIesire",6060);
    }


}
